import firebase from 'firebase/app';
import 'firebase/firestore';

// Initializing Firebase...
var config = {
  apiKey: "AIzaSyDKqCsW_dmo2qdG3jfDCO7VUNdo7lgYybE",
  authDomain: "logger-3bde6.firebaseapp.com",
  databaseURL: "https://logger-3bde6.firebaseio.com",
  projectId: "logger-3bde6",
  storageBucket: "logger-3bde6.appspot.com",
  messagingSenderId: "293975192111"
};

firebase.initializeApp(config);

const db = firebase.firestore();

db.settings({ timestampsInSnapshots: true });

export default db;