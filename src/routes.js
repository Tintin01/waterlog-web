import Logs from './views/Logs'
import Stats from './views/Stats'
import Account from './views/Account'

export const routes = [
  { path: '/', component: Logs, name: 'logs' },
  { path: '/statistics', component: Stats, name: 'statistics' },
  { path: '/account', component: Account, name: 'account' }
]