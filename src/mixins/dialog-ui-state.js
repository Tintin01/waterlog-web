export default {
  data() {
    return {
      showDialog: false,
      isDisabled: true,
      addButtonLoader: false,
      readingRules: [
        val => {
          if (this.newReading) {
            return !isNaN(val) ? true : "reading must be a number";
          }
          return "invalid reading";
        },
        val => {
          if (this.newReading) {
            if (val.length === 7 && !isNaN(val)) {
              this.isDisabled = false;
              return true;
            } else if (this.newReading !== 7) {
              return 'use the first 7 digits on your water meter';
            }
          }
          return 'invalid entry'
        }
      ],
    }
  },
  methods: {
    onDialogClosed() {
      this.showDialog = false
      this.newReading = null;
      this.isDisabled = true;
    },
    checkReadingLength() {
      if (this.newReading) {
        return this.newReading.length === 7 ? true : false;
      }
    }
  }
}