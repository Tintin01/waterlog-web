import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import Vuetify from 'vuetify/lib'
import { routes } from './routes'

Vue.config.productionTip = false
Vue.use(VueRouter);
Vue.use(VueChartkick, { adapter: Chart });

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.use(Vuetify, {
    primary: '#1976D2',
    secondary: '#1976D2',
    accent: '#1976D2',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
);

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
